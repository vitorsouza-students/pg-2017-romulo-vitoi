if (config('app.mode') === 'sispreco') {
    Route::prefix('api')
        ->middleware('api')
        ->namespace($this->namespace)
        ->group(base_path('routes/sispreco_api.php'));
}
