class BeneficiarioController extends Controller
{
    public function all()
    {
        return BeneficiarioResource::collection(Beneficiario::all());
    }
}