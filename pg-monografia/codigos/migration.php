class CreateProdutosLevantamentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos_levantamento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('produto');
            $table->integer('municipio');
            $table->integer('ano');

            $table->unique(['produto', 'municipio', 'ano']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos_levantamento');
    }
}