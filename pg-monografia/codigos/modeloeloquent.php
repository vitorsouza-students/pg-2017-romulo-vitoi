class Producao extends Eloquent\Model
{
    protected $table = 'producao';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'produto',
        'municipio',
        'quantidade',
        'ano',
        'mes',
        'tipo',
    ];
}