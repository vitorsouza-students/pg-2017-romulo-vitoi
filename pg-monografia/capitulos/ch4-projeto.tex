% ==============================================================================
% TCC - Romulo de Angelis Vitoi
% Capítulo 3 - Projeto Arquitetural e Implementação
% ==============================================================================
\chapter{Projeto e Implementação}
\label{sec-projeto}

Este capítulo tem como objetivo apresentar o que foi desenvolvido, a solução proposta para o problema identificado e modelado no levantamento de requisitos. Para isso se faz necessário ter, além do conhecimento do domínio, também ter o conhecimento das tecnologias que serão utilizadas e a arquitetura a ser utilizada~\cite{falboProjeto}.

As seções seguintes descrevem como o SisPreço foi construído. A Seção~\ref{sec-projeto-arquitetura} apresenta a arquitetura do sistema, bem como todas as tecnologias envolvidas e sua implementação. A Seção~\ref{sec-projeto-frameweb} apresenta os modelos do FrameWeb criados. Por fim, a Seção~\ref{sec-projeto-apresentacao} apresenta os resultados obtidos através de capturas de tela.


\section{Arquitetura e Implementação}
\label{sec-projeto-arquitetura}

O SisPreço é composto por três subsistemas, dois subsistemas para o lado do servidor e outro para o lado do cliente. Para os dois subsistemas referentes ao lado do servidor, desenvolvidos para serem APIs, é usado o \textit{framework} Laravel da linguagem PHP. O \textit{framework} ORM (\textit{Object/Relational Mapping}) usado foi o Eloquent. Já do lado do cliente é usado o Vue.js, um \textit{framework} JavaScript para construção de interfaces com o usuário.

Para esse projeto foi utilizado o sistema de gerenciamento de banco de dados MySQL. A interação com ele é inteiramente responsabilidade dos subsistemas do lado do servidor, sendo manipulados através de modelos Eloquent.

A arquitetura do serviço é representada na Figura~\ref{figura-arquitetura-servico}. Por trás de um \textit{firewall}, a API do Incaper se comunica com o banco de dados para retornar dados para a API do SisPreço, através da Internet. O SisPreço, por sua vez, se comunica com seu banco de dados para a leitura e persistência de dados e utiliza um banco de dados secundário para fazer o cache de valores que ele requisita da API do Incaper.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{figuras/arquitetura/arquitetura_servico.pdf}
	\caption{Arquitetura do serviço.}
	\label{figura-arquitetura-servico}
\end{figure}

A Listagem~\ref{dockercompose} mostra um exemplo simplificado de como essa arquitetura foi representada para a execução do sistema localmente utilizando o Docker.

\lstinputlisting[caption={Exemplo simplificado do arquivo docker-compose.yml}, frame=single, label={dockercompose}, float]{codigos/dockercompose.yml}

O modelo escolhido para a construção da interface \textit{Web} foi de uma aplicação de página única. Sua diferença para as aplicações de páginas tradicionais, ou de multi-páginas, é que toda a estrutura necessária pra a renderização da página e de seus dados é obtida de uma só vez, navegações entre seções da página se dão pela alteração do conteúdo exibido. Caso seja necessário o envio ou obtenção de dados, é feita uma requisição AJAX que retorna, normalmente, dados formatados em JSON. Em páginas tradicionais, a navegação se dá por meio de uma outra requisição ao servidor que retorna uma nova página. Os ciclos de vida desses dois tipos de aplicações \textit{Web} são mostrados na Figura~\ref{figura-ciclo-vida-paginas}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/arquitetura/paginas.pdf}
	\caption{Ciclo de vida de páginas tradicionais e de aplicações de página única.}
	\label{figura-ciclo-vida-paginas}
\end{figure}

A utilização do \textit{framework} Vue.js facilitou muito a construção de uma aplicação de página única, utilizando suas funcionalidades de renderização condicional e de templates, demonstradas nas Listagens~\ref{vueif} e~\ref{vuetemplate}.

\lstinputlisting[caption={Renderização condicional no Vue.js}, float, frame=single, label={vueif}]{codigos/if.vue}

\lstinputlisting[caption={Renderização de templates no Vue.js}, float, frame=single, label={vuetemplate}]{codigos/table.vue}

Apesar de serem subsistemas diferentes, as APIs do SisPreço e Incaper compartilham de alguns modelos, de códigos e configurações em comum para a inicialização do \textit{framework}, e por este motivo foram implementados utilizando a mesma base de código. Em situações onde se viu necessária uma diferente implementação para os subsistemas, foram utilizadas estruturas condicionais baseadas no valor de uma variável de ambiente. A Listagem~\ref{toggle} mostra o caso da inclusão das rotas da API do SisPreço apenas se o modo de execução for \emph{sispreco}.

\lstinputlisting[caption={Inclusão das rotas da API do SisPreço}, float, frame=single, label={toggle}]{codigos/toggle.php}

A Listagem~\ref{endpointsincaper} mostra exemplos de como são definidas algumas rotas da API do Incaper, o mesmo é feito para as rotas do SisPreço. Através do módulo Route do Laravarel, é possível definir as rotas de uma maneira simples e expressiva. Por exemplo, a linha 1 da Listagem~\ref{endpointsincaper} define uma rota do método \emph{GET} para o caminho \emph{/municipios} e indica que ela deve ser tratada pelo método \emph{all} da classe \emph{MunicipioController}.

\lstinputlisting[caption={Exemplos de definições das rotas da API do Incaper}, float, frame=single, label={endpointsincaper}]{codigos/endpointsincaper.php}

Os controladores criados durante a implementação do sistemas são mostrados na Figura~\ref{figura-pasta-controllers}, eles são responsáveis pelo tratamento das requisições recebidas. A Listagem~\ref{controller} apresenta a implementação de uma ação que apresenta todos os beneficiários cadastrados no banco de dados.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.35\textwidth]{figuras/arquitetura/controllers.png}
	\caption{Estrutura da pasta \emph{app/Http/Controllers}.}
	\label{figura-pasta-controllers}
\end{figure}

\lstinputlisting[caption={Controlador resposável pelos Beneficiários da API do Incaper}, float, frame=single, label={controller}]{codigos/controller.php}

A Figura~\ref{figura-pasta-models} apresenta os modelos implementados no sistema.
A implementação do modelo \emph{Produto} é mostrada na Listagem~\ref{modeloeloquent} para demonstrar a implementação dos modelos. A Listagem~\ref{sumquantidade} mostra como é simples realizar uma operação a partir de um modelo Eloquent, neste exemplo é obtida a soma das produções de um determinado produto e um ano.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.35\textwidth]{figuras/arquitetura/models.png}
	\caption{Estrutura da pasta \emph{app/Models}.}
		\label{figura-pasta-models}
\end{figure}

\lstinputlisting[caption={Modelo \emph{Producao}}, float, frame=single, label={modeloeloquent}]{codigos/modeloeloquent.php}

\lstinputlisting[caption={Utilização dos métodos do Eloquent}, float, frame=single, label={sumquantidade}]{codigos/sumquantidade.php}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{figuras/arquitetura/migrations.png}
	\caption{Estrutura da pasta \emph{app/database}.}
	\label{figura-pasta-migrations}
\end{figure}

A Figura~\ref{figura-pasta-migrations} mostra a estrutura da pasta \emph{app/database} que contém apenas a pasta de migrações do banco de dados. As migrações são mudanças que devem ser feitas na estrutura do banco de dados para acomodar as mudanças que ocorreram em uma nova versão do código. O Laravel mantém uma tabela no banco de dados para fazer o controle do versionamento, a versão do banco é dada pela última migração que ele executou e é definida pela data de criação da migração no início do nome do arquivo.

Apesar de ser possível definir migrações usando a linguagem SQL, a classe \emph{Migration} do pacote \emph{Database} do Laravel provê uma API para a manipulação das migrações. A Listagem~\ref{migration} contém um exemplo utilizado no sistema para a criação de uma nova tabela. O método \emph{up} é executado ao realizar a migração, criando a tabela \emph{produtos\_levantamento}, já o método \emph{down} é executado caso haja a necessidade de reverter a migração, removendo a tabela criada.

\lstinputlisting[caption={Exemplo de uma migração do banco do dedados}, float, frame=single, label={migration}]{codigos/migration.php}

Apesar de não ser uma parte muito relevante do sistema, o cliente para Android serve como uma prova de conceito de que sistemas contruídos totalmente em cima de APIs podem ser fácilmente portados para outras plataformas, já que a implementação do cliente é totalmente independente da implementação do sistema e pode se comunicar com ele através de protocolos já consolidados.

Os códigos-fonte dos sistemas estão disponíveis no repositório do Incaper: \url{https://gitlab.com/incaper/}.

\section{Modelos FrameWeb}
\label{sec-projeto-frameweb}

Nesta seção, serão apresentados diagramas no padrão proposto pelo \textit{FrameWeb}~\cite{frameweb} de forma adaptada ao \textit{framework} Laravel sendo utilizado em uma aplicação de página única.

O Modelo de Domínio é um diagrama de classes da UML que representa os objetos de domínio do problema e seu mapeamento para a persistência em um banco de dados relacional. A partir dele são implementadas as classes da camada de Domínio.

Diferentemente da abordagem original proposta em 2007, todos os atributos que não podem ser nulos tiveram a tag \texttt{not null} omitida e aqueles que podem tiveram a tag \texttt{null} adicionada de forma a reduzir o impacto visual nos diversos diagramas. Pelo mesmo motivo, foi considerada que a estratégia padrão de recuperação de uma associação é do tipo \textit{lazy}, e não \textit{eager} como proposto pelo FrameWeb.

Todas as classes de domínio estendem uma classe do pacote Eloquent do Laravel, responsável pelo mapeamento entre classes e tabelas, por associações, dentre outras funcionalidades. Essa herança não é mostrada nos diagramas com o intuito de não poluí-los com várias associações, mas é possível saber mais sobre o Eloquent em \url{https://laravel.com/docs/5.6/eloquent}.

O modelo de domínio do sistema foi dividido em dois subsistemas pois os dados que são providos pelo Incaper são armazenados em um banco de dados diferente do SisPreço. A Figura~\ref{figura-dominio-incaper} representa o modelo de domínio do subsistema Incaper.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/projeto/dominioincaper.pdf}
	\caption{Modelo de Domínio do susbsistema Incaper.}
	\label{figura-dominio-incaper}
\end{figure}

A seguir, a Figura~\ref{figura-dominio-sispreco} representa o modelo de domínio do subsistema SisPreço, onde as classes \textit{Beneficiário}, \textit{Produto} e \textit{Município} são apenas referências para classes que existem no subsistema Incaper.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/projeto/dominiosispreco.pdf}
	\caption{Modelo de Domínio do subsistema SisPreço.}
	\label{figura-dominio-sispreco}
\end{figure}

O Modelo de Persistência é um diagrama de classes da UML que representa as classes DAO existentes, responsáveis pela persistência das instâncias das classes de domínio. Diferente do que indica o método FrameWeb, não foram utilizadas classes DAO para a camada de acesso a dados, pois os modelos, como mencionado anteriormente, estendem um modelo Eloquent que já provê todas as funcionalidades necessárias para persistência dos dados, e portanto, não foram modelados os modelos de persistência. A Figura~\ref{figura-eloquent} apresenta a classe abstrata Eloquent, com alguns de seus métodos relevantes para a obtenção e persistência dos dados.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.55\textwidth]{figuras/projeto/persistenciabase.pdf}
	\caption{Classe abstrata Eloquent.}
	\label{figura-eloquent}
\end{figure}

O Modelo de Navegação é um diagrama de classe da UML que representa os diferentes componentes que formam a camada de Apresentação, como páginas \textit{Web}, formulários HTML e classes de ação. Esse modelo é utilizado pelos desenvolvedores para guiar a codificação das classes e componentes das camadas de View e Controller. A classe de ação é o principal componente deste modelo: suas associações de dependência ditam o controle de fluxo quando uma ação é executada.

Por estarmos lidando com uma aplicação de página única, foi adotada a nomenclatura \texttt{/\#fragmento} para descrever qual o fragmento da página está sendo exibido. Todos os métodos representados nos modelos de navegação a seguir são chamados por requisições AJAX e o retorno de dados é sempre por objetos JSON.

A Figura~\ref{figura-navegacao-relatorios} apresenta o modelo de navegação para o fluxo \emph{Acessar relatórios de preço detalhado} do caso de uso \emph{Acessar relatórios de preço detalhado}. O sistema, ao exibir o fragmento de consulta de preços, executa os métodos \emph{getProdutos} e \emph{getMunicipios}, que são necessários para popular as opções do formulário. Na submissão do formulário, o método \emph{gerar} é executado, retornando os dados do relatório para a exibição.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{figuras/projeto/navegacao2.pdf}
	\caption{Modelo de Navegação - Visualização de relatórios.}
	\label{figura-navegacao-relatorios}
\end{figure}
	
A Figura~\ref{figura-navegacao-levantamento-intervalo} exibe os modelos de navegação para os fluxos \emph{Visualizar intervalos de preços} e \emph{Definir intervalos de preços} do caso de uso \emph{Gerenciar intervalos de preços} e o fluxo \emph{Realizar levantamento de preços} do caso de uso \emph{Realizar levantamento de preços}. Os fragmentos \emph{levantamento} e \emph{intervalo}, assim como o exemplo anterior, necessitam de dados adicionais para popular formulários ou listas. No fragmento \emph{levantamento}, os métodos \emph{getProdutos} e \emph{getFromMunicipio} são executados para a obtenção dos produtos participantes no levantamento de um município e os beneficiários de um município, respectivamente. Ainda no fragmento \emph{levantamento}, a submissão do formulário de levantamento de preços executa o método \emph{create} para enviar os dados para o sistema. O fragmento \emph{intervalo} executa o método \emph{getIntervaloPreco} para obter os intervalos de preços e \emph{postIntervaloPreco} ao submeter o formulário para atualizar um intervalo.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/projeto/navegacao.pdf}
	\caption{Modelo de Navegação - Levantamento e definição de intervalo de preços.}
	\label{figura-navegacao-levantamento-intervalo}
\end{figure} 

Por fim, a Figura~\ref{figura-navegacao-importar} apresenta o modelo de navegação para o fluxo \emph{Importar dados de produção} do caso de uso \emph{Importar dados de produção}.  Ele se inicia ao acessar a página de importação de dados de produção, representada pelo fragmento \emph{producao}, neste fragmento tem-se a opção de fazer a importação dos dados enviando um arquivo com os dados de produção e selecionando o tipo do relatório. A importação é dada pelos métodos \emph{pevs}, \emph{ppm} e \emph{lspa}, nomeados de acordo com o tipo do relatório selecionado. O resultado contém a informação se a importação foi bem sucedida e a lista de erros, caso contrário.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{figuras/projeto/navegacao3.pdf}
	\caption{Modelo de Navegação - Importação de dados de produção.}
	\label{figura-navegacao-importar}
\end{figure}




\section{Apresentação}
\label{sec-projeto-apresentacao}

Nesta seção o sistema é apresentado por meio de capturas de tela, a fim de demonstrar o resultado obtido.

\subsection{Sistema Web}

Ao clicar em \textit{Entrar no sistema} o usuário é redirecionado para o provedor de identidades do Incaper. A Figura~\ref{tela-incaper-login} exibe a tela de login do provedor de identidades, nela o usuário deve inserir suas credenciais e, em caso de sucesso, é redirecionado de volta para o SisPreço. Caso o usuário não lembre sua senha, é possível recuperá-la através do formulário de recuperação de senha, mostrado na Figura~\ref{tela-incaper-recuperar}. Após preencher o formulário o usuário receberá um e-mail com as instruções para recuperar sua senha. Ao acessar o \textit{link} de recuperação por \textit{e-mail}, o usuário terá acesso à tela demonstrada na Figura~\ref{tela-incaper-alterar} onde será possível alterar sua senha.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\textwidth]{figuras/apresentacao/login.png}
    \caption{SisPreço - \textit{Login}.}
    \label{tela-incaper-login}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\textwidth]{figuras/apresentacao/esqueceu_senha.png}
    \caption{SisPreço - Recuperar senha.}
    \label{tela-incaper-recuperar}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\textwidth]{figuras/apresentacao/alterar_senha.png}
    \caption{SisPreço - Alterar senha.}
    \label{tela-incaper-alterar}
\end{figure}

O sistema conta com um menu de navegação horizontal que, ao passar o mouse sobre seus itens, exibe as demais opções relacionadas, este comportamento é mostrado na Figura~\ref{tela-menu}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{figuras/apresentacao/menu.png}
	\caption{SisPreço - Menu.}
	\label{tela-menu}
\end{figure}

A Figura~\ref{tela-intervalo-precos} apresenta a tela dos parâmetros de preços mínimos e máximos dos produtos do sistema. Esta tela exibe a lista de todos os produtos presentes no sistema, juntamente com sua unidade de medida e os valores definidos para o preço mínimo e máximo. Ao clicar no botão \textit{Editar} de um produto, seus campos de preço mínimo e máximo se tornam editáveis e é possível alterar os valores, o botão \textit{Editar} é transformado em um botão \textit{Salvar} que ao ser clicado salva os valores definidos.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/apresentacao/parametros_preco.png}
	\caption{SisPreço - Intervalo de Preços.}
	\label{tela-intervalo-precos}
\end{figure}

A Figura~\ref{tela-importar} apresenta a tela de importação de dados de produção. O usuário deve selecionar um arquivo CSV de seu computador que contenha os dados de produção a serem importados e deve também selecionar o tipo de relatório que está sendo importado. Caso ocorra algum erro durante a importação, o usuário é apresentado à lista de erros que ocorreram e tem a opção de prosseguir com a importação ignorando as entradas que apresentaram erros, como mostra a Figura~\ref{tela-importar-erro}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/apresentacao/importar.png}
	\caption{SisPreço - Importar dados de produção.}
	\label{tela-importar}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/apresentacao/importar_erros.png}
	\caption{SisPreço - Erro ao importar dados.}
	\label{tela-importar-erro}
\end{figure}

A Figura~\ref{tela-definicao-produtos} exibe a tela de definição de produtos a participarem do levantamento de preços em um município. Ao selecionar um município a partir das opções, é carregada a lista de produtos, cada um acompanhando de uma caixa de seleção que determina se o mesmo deve participar do levantamento. Ao clicar em \textit{Salvar}, os dados são salvos.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/apresentacao/definicao_levantamento.png}
	\caption{SisPreço - Definição de produtos.}
	\label{tela-definicao-produtos}
\end{figure}

A Figura~\ref{tela-levantamento} apresenta a tela de levantamento de preços. O usuário deve selecionar o município, produto e produtor a partir da lista de opções e inserir o preço do produto. Ao clicar em \textit{Enviar} os dados são enviados ao servidor e os campos são limpos para que possa ser realizado um novo levantamento. Caso o preço do produto fornecido pelo usuário esteja fora do intervalo de preços aceitáveis, ele será alertado, como demonstrado.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/apresentacao/levantamento.png}
	\caption{SisPreço - Levantamento de preços.}
	\label{tela-levantamento}
\end{figure}

A Figura~\ref{tela-consulta-levantamento} exibe a tela de consulta aos levantamentos de preços realizados. O usuário, ao preencher os campos, pode consultar qual Servidor realizou um levantamento de preços específico.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/apresentacao/consulta_levantamento.png}
	\caption{SisPreço - Consulta de levantamento de preço.}
	\label{tela-consulta-levantamento}
\end{figure}

As Figuras~\ref{tela-consulta-detalhada} e~\ref{tela-serie-historica} apresentam as tela de geração de relatórios de séries históricas e de preços detalhados, respectivamente. O usuário pode personalizar os parâmetros e escolher se deseja exibir o resultado na interface do sistema ou exportá-lo para um arquivo CSV.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/apresentacao/consulta_detalhada.png}
	\caption{SisPreço - Consulta de preços detalhados.}
	\label{tela-consulta-detalhada}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/apresentacao/serie_historica.png}
	\caption{SisPreço - Consulta de série histórica de preço.}
	\label{tela-serie-historica}
\end{figure}


\subsection{Aplicativo Android}

Esta seção apresenta as capturas de tela da aplicação móvel para Android.

A Figura~\ref{tela-android-login} apresenta a tela inicial do sistema quando o usuário ainda não está autenticado. Ao clicar no botão \textit{ENTRAR}, o usuário é encaminhado para uma página Web externa ao aplicativo onde deve se autenticar no provedor de identidades do Incpaper, em caso de sucesso na autenticação o usuário é redirecionado de volta ao aplicativo e é apresentado à tela de menu inicial.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{figuras/apresentacao/android_login.png}
    \caption{Aplicativo Android - \textit{Login}.}
    \label{tela-android-login}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\textwidth]{figuras/apresentacao/android_menu.png}
	\caption{Aplicativo Android - Menu inicial.}
	\label{tela-android-menu}
\end{figure}

Na Figura~\ref{tela-android-menu} é apresentada a tela de menu inicial, esta tela é acessada após a realização do \textit{login}, no caso do usuário já ter realizado o \textit{login} no aplicativo anteriormente, esta é a tela inicial. Nesta tela é exibida a única funcionalidade atualmente presente no aplicativo, o levantamento de preços. Ao clicar em uma opção o usuário é redirecionado para a respectiva tela.

A Figura~\ref{tela-android-levantamento} apresenta a tela de lavantamento de preços, onde o usuário deve preencher quatro campos para realizar o levantamento, os campos \textit{Município}, \textit{Produto} e \textit{Produtor} são campos auto-completáveis, que exibem as possibilidades de valores a serem selecionados de acordo com o que o usuário digita. Ao selecionar o botão \textit{INCLUIR} uma requisição é enviada ao servidor com as informações e os campos são limpos para que o usuário possa realizar um novo levantamento.
A seta no canto superior esquerdo retorna o usuário para a tela anterior.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\textwidth]{figuras/apresentacao/android_levantamento.png}
	\caption{Aplicativo Android - Levantamento de Preços.}
	\label{tela-android-levantamento}
\end{figure}

