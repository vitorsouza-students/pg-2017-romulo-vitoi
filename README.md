# README

Projeto de Graduação de Rômulo de Angelis Vitoi: _SisPreço: Sistema de Preços do Incaper_.

### Resumo ###

Semanalmente, o Incaper (Instituto Capixaba de Pesquisa, Assistência Técnica e Extensão Rural) realiza levantamentos de preços de produtos agrícolas, de pecuária e de silvicultura em diversos municípios do estado do Espírito Santo. Além de servir como fonte de consulta para os produtores rurais e comerciantes, tais levantamentos são instrumentos balizadores de mercado, cujos preços levantados são utilizados como referência por diversos programas governamentais no estado, como o Programa de Aquisição da Agricultura Familiar (PAA) e o Programa de Garantia de Preço Mínimo (PGPM). O levantamento também é utilizado pelo Instituto Brasileiro de Geografia e Estatística (IBGE) e Instituto Jones dos Santos Neves (IJSN) para cálculo do PIB (produto interno bruto) trimestral e anual do estado, bem como para o cálculo do PIB anual dos municípios. Atualmente estes dados de preços não estão sistematizados e não são de fácil acesso, dificultando sua manipulação. O SisPreço surge como uma solução para organizar os dados de uma maneira centralizada onde é possível manipulá-los mais facilmente.

Para a construção do sistema, foram colocadas em prática as disciplinas aprendidas ao decorrer do curso, tais como Programação, Linguagens de Programação, Banco de Dados, Engenharia de Software e Estruturas de Dados.
